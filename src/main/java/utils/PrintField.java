package utils;

import org.protelis.lang.datatype.Field;

public class PrintField {

	public static String fieldToString(Field<Object> field) {
		return field.toMap().toString();
	}
}
